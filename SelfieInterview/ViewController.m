//
//  ViewController.m
//  SelfieInterview
//
//  Created by Michael Rivas on 4/15/15.
//  Copyright (c) 2015 Michael Rivas. All rights reserved.
//

#import "ViewController.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTLYouTube.h"


static NSString *const kKeychainItemName = @"OAuth2: SelfieInterview";

#define YOUTUBE_CLIENT_ID @"899607392837-bgu5hf6sjv5i5njmkoebqcsi17sukrds.apps.googleusercontent.com"
#define YOUTUBE_CLIENT_SECRET @"VUz2nLoKlVf27TKjnFhBmgPy"

@interface ViewController ()
@property (nonatomic, readonly) GTLServiceYouTube *youTubeService;
@property (nonatomic, weak) IBOutlet UILabel *usernameLabel;
@end

@implementation ViewController

- (void)awakeFromNib {
	GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
																						  clientID:YOUTUBE_CLIENT_ID
																					  clientSecret:YOUTUBE_CLIENT_SECRET];
	self.youTubeService.authorizer = auth;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	self.usernameLabel.text = [self signedInUsername];
	
	// Show the OAuth 2 sign-in controller.
	if ([self.usernameLabel.text isEqualToString:@""]) {
		GTMOAuth2ViewControllerTouch *viewController = [GTMOAuth2ViewControllerTouch controllerWithScope:[NSString stringWithFormat:@"%@ %@", kGTLAuthScopeYouTubeUpload, kGTLAuthScopeYouTube]
																								clientID:YOUTUBE_CLIENT_ID
																							clientSecret:YOUTUBE_CLIENT_SECRET
																						keychainItemName:kKeychainItemName
																								delegate:self
																						finishedSelector:@selector(viewController:finishedWithAuth:error:)];
		
		[[self navigationController] pushViewController:viewController animated:YES];
		
	}
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
	  finishedWithAuth:(GTMOAuth2Authentication *)auth
				 error:(NSError *)error {
 if (error != nil) {
	 NSLog(@"Auth failed!");
 } else {
 	 self.usernameLabel.text = [self signedInUsername];
 }
}
#pragma mark - Property methods
- (GTLServiceYouTube *)youTubeService {
	static GTLServiceYouTube *service;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		service = [[GTLServiceYouTube alloc] init];
		
		// Have the service object set tickets to fetch consecutive pages
		// of the feed so we do not need to manually fetch them.
		service.shouldFetchNextPages = YES;
		
		// Have the service object set tickets to retry temporary error conditions
		// automatically.
		service.retryEnabled = YES;
	});
	return service;
}
#pragma mark - Helper methods
- (NSString *)signedInUsername {
	// Get the email address of the signed-in user.
	GTMOAuth2Authentication *auth = self.youTubeService.authorizer;
	BOOL isSignedIn = auth.canAuthorize;
	if (isSignedIn) {
		return auth.userEmail;
	} else {
		return @"";
	}
}

- (BOOL)isSignedIn {
	NSString *name = [self signedInUsername];
	return (name != nil);
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
